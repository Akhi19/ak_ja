CREATE TABLE users (
	email varchar(100),
	address TEXT,
	telephone TEXT,
	name TEXT,
	password TEXT,
	googlesignin BOOLEAN,
	PRIMARY KEY(email)
);

CREATE TABLE resetTokens(
	email TEXT,
	token TEXT,
	expirytime INT,
	used BOOLEAN
);

CREATE TABLE session_token(
	email TEXT,
	token TEXT
);