package service

import (
	"ak_ja/src/config"
	"ak_ja/src/model"
	"ak_ja/src/service/dal/mocks"
	"errors"
	"reflect"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
)

func TestAuthenticateAndGetUser(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	dbMock := mock_dal.NewMockLoginServiceDal(ctrl)
	service := &LoginService{
		db:      dbMock,
		configs: &config.Configuration{},
	}
	tt := []struct {
		name         string
		email        string
		password     string
		expectedErr  error
		dbRecord     []map[string]interface{}
		dbError      error
		expectedUser *model.User
	}{
		{
			name:        "Valid User",
			email:       "abc@xyz.com",
			password:    "1234",
			expectedErr: nil,
			dbRecord: []map[string]interface{}{
				map[string]interface{}{
					"email":     "abc@xyz.com",
					"password":  "1234",
					"address":   "123",
					"name":      "123",
					"telephone": "123"},
			},
			dbError: nil,
			expectedUser: &model.User{
				Email:     "abc@xyz.com",
				Password:  "1234",
				Address:   "123",
				Name:      "123",
				Telephone: "123",
			},
		},
		{
			name:        "Wrong User Password",
			email:       "abc@xyz.com",
			password:    "1234",
			expectedErr: errors.New("Wrong Password"),
			dbRecord: []map[string]interface{}{
				map[string]interface{}{
					"email":     "abc@xyz.com",
					"password":  "12345",
					"address":   "123",
					"name":      "123",
					"telephone": "123"},
			},
			dbError:      nil,
			expectedUser: nil,
		},
	}
	for _, test := range tt {
		t.Run(test.name, func(t *testing.T) {
			dbMock.EXPECT().GetUser(test.email).Return(test.dbRecord, test.dbError).Times(1)
			user, err := service.AuthenticateAndGetUser(test.email, test.password)
			assert.Equal(t, test.expectedErr, err, "Expected and actual error dont match")
			assert.True(t, reflect.DeepEqual(user, test.expectedUser), "Expected and actual User object dont match")
		})
	}
}
