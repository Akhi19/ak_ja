package service

import (
	"ak_ja/src/common"
	"ak_ja/src/config"
	"ak_ja/src/model"
	"ak_ja/src/service/dal"
	"errors"
	"fmt"
	"time"
)

const (
	resetEmailBody = `Password Reset link : %s.
This link is valid only for 15 mins`
)

var (
	getConfig      = config.GetConfig
	getServiceDal  = dal.GetLoginServiceDal
	getEmailSender = common.NewEmailSender
)

type (
	LoginService struct {
		db      dal.LoginServiceDal
		configs *config.Configuration
	}
)

func GetLoginService() (*LoginService, error) {
	configs := getConfig()
	db, err := getServiceDal(configs)
	if err != nil {
		return nil, err
	}
	return &LoginService{
		db:      db,
		configs: configs,
	}, nil
}

func (service *LoginService) AuthenticateAndGetUser(email, password string) (*model.User, error) {
	user, err := service.GetUser(email)
	if err != nil {
		return nil, err
	}
	if user.Password != password {
		return nil, errors.New("Wrong Password")
	}
	return user, nil
}

func (service *LoginService) SignUp(m map[string]interface{}, googleSignIn bool) error {
	return service.db.RegisterUser(m, googleSignIn)
}

func (service *LoginService) UpdateProfile(m map[string]interface{}, currentEmail string) error {
	return service.db.UpdateProfile(m, currentEmail)
}

func (service *LoginService) SaveSessionToken(token, email string) error {
	return service.db.SaveSessionToken(token, email)
}

func (service *LoginService) DeleteSessionToken(token string) error {
	return service.db.DeleteSessionToken(token)
}

func (service *LoginService) GetUserFromSessionToken(token string) (*model.User, error) {
	user, err := service.db.GetUserFromSessionToken(token)
	if err != nil {
		return nil, err
	}
	if len(user) == 0 {
		return &model.User{}, errors.New("Email does not exist")
	}
	userModel := &model.User{}
	userModel.PopulateModel(user[0])
	return userModel, nil
}

func (service *LoginService) GetUser(email string) (*model.User, error) {
	user, err := service.db.GetUser(email)
	if err != nil {
		return nil, err
	}
	if len(user) == 0 {
		return &model.User{}, errors.New("Email does not exist")
	}
	userModel := &model.User{}
	userModel.PopulateModel(user[0])
	return userModel, nil
}

func (service *LoginService) ForgotPassword(email string) error {
	_, err := service.GetUser(email)
	if err != nil {
		return err
	}
	expiration := time.Now().UnixNano()/1000000000 + int64(service.configs.ResetLinkExpirationSecs)
	token := common.GenerateRandomToken()
	err = service.db.SaveToken(email, token, expiration)
	if err != nil {
		return err
	}
	tokenUrl := service.configs.HostIP + service.configs.UIServer.ListenURL + service.configs.UIServer.URLPrefix +
		service.configs.UIServer.APIVersion + "/resetpasswordpage?tokenid=" + token + "&email=" + email
	return service.sendEmail(email, fmt.Sprintf(resetEmailBody, tokenUrl))
}

func (service *LoginService) sendEmail(email, emailBody string) error {
	emailSender := getEmailSender(service.configs.EmailSettings)
	return emailSender.Send([]string{email}, []byte(emailBody))
}

func (service *LoginService) VerifyToken(token, email string) error {
	tokenDetails, err := service.db.GetTokenDetails(email, token)
	if err != nil {
		return errors.New("Connection Error")
	}
	if tokenDetails.Used ||
		tokenDetails.ExpiryTime < time.Now().UnixNano()/1000000000 {
		return errors.New("Token Expired")
	}
	err = service.db.MarkTokenUsed(email, token)
	if err != nil {
		return errors.New("Retry")
	}
	return nil
}

func (service *LoginService) UpdatePassword(email, password string) error {
	return service.db.UpdatePassword(email, password)
}
