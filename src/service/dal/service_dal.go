package dal

import (
	"ak_ja/src/config"
	"ak_ja/src/dal"
	"ak_ja/src/model"
	"fmt"
)

type LoginServiceDal interface {
	GetUser(email string) ([]map[string]interface{}, error)
	GetUserFromSessionToken(token string) ([]map[string]interface{}, error)
	RegisterUser(m map[string]interface{}, googleSignIn bool) error
	UpdateProfile(m map[string]interface{}, currentEmail string) error
	SaveToken(email, token string, expiration int64) error
	GetTokenDetails(email, token string) (model.ResetToken, error)
	MarkTokenUsed(email, token string) error
	UpdatePassword(email, password string) error
	SaveSessionToken(token, email string) error
	DeleteSessionToken(token string) error
}

type loginServiceDal struct {
	db      dal.DbAccessor
	configs *config.Configuration
}

func GetLoginServiceDal(configs *config.Configuration) (LoginServiceDal, error) {
	db, err := dal.ConnectDB(configs)
	if err != nil {
		return nil, err
	}
	return &loginServiceDal{
		db:      db,
		configs: configs,
	}, nil
}

func (loginDal *loginServiceDal) GetUser(email string) ([]map[string]interface{}, error) {
	return loginDal.db.SelectQuery(getUserQuery, email)
}

func (loginDal *loginServiceDal) GetUserFromSessionToken(token string) ([]map[string]interface{}, error) {
	return loginDal.db.SelectQuery(getUserFromSessionTokenQuery, token)
}

func (loginDal *loginServiceDal) RegisterUser(m map[string]interface{}, googleSignIn bool) error {
	return loginDal.db.InsertQuery(registerUser, fmt.Sprintf("%s", m["password"]), fmt.Sprintf("%s", m["email"]), googleSignIn)
}

func (loginDal *loginServiceDal) UpdateProfile(m map[string]interface{}, currentEmail string) error {
	return loginDal.db.InsertQuery(updateProfile, fmt.Sprintf("%s", m["email"]), fmt.Sprintf("%s", m["address"]),
		fmt.Sprintf("%s", m["telephone"]), fmt.Sprintf("%s", m["name"]), currentEmail)
}

func (loginDal *loginServiceDal) SaveToken(email, token string, expiration int64) error {
	return loginDal.db.InsertQuery(saveTokenQuery, email, token, expiration)
}

func (loginDal *loginServiceDal) GetTokenDetails(email, token string) (model.ResetToken, error) {
	t := model.ResetToken{}
	m, err := loginDal.db.SelectQuery(getTokenQuery, email, token)
	if err != nil {
		return t, err
	}
	if len(m) == 0 {
		return t, fmt.Errorf("No token record found")
	}
	t.PopulateModel(m[0])
	return t, nil
}

func (loginDal *loginServiceDal) MarkTokenUsed(email, token string) error {
	return loginDal.db.InsertQuery(markTokenUsed, email, token)
}

func (loginDal *loginServiceDal) UpdatePassword(email, password string) error {
	return loginDal.db.InsertQuery(updatePassword, password, email)
}

func (loginDal *loginServiceDal) SaveSessionToken(token, email string) error {
	return loginDal.db.InsertQuery(saveSessionToken, token, email)
}

func (loginDal *loginServiceDal) DeleteSessionToken(token string) error {
	return loginDal.db.DeleteQuery(deleteSessionToken, token)
}
