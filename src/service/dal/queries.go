package dal

const (
	getUserQuery                 = "SELECT * FROM users WHERE email = ?"
	registerUser                 = "INSERT INTO users (password,email,googlesignin) VALUES (?,?,?)"
	saveTokenQuery               = "INSERT INTO resetTokens(email,token,expiryTime,used) VALUES (?,?,?,false)"
	getTokenQuery                = "SELECT * FROM resetTokens where email=? AND token = ?"
	markTokenUsed                = "UPDATE resetTokens SET used = true WHERE email = ? AND token = ?"
	updatePassword               = "UPDATE users SET password = ? WHERE email = ?"
	updateProfile                = "UPDATE users SET email = ?, address = ?, telephone = ?, name = ? WHERE email = ?"
	saveSessionToken             = "INSERT INTO session_token (token,email) VALUES (?,?)"
	getUserFromSessionTokenQuery = "SELECT * FROM users WHERE email = (SELECT email FROM session_token WHERE token = ?)"
	deleteSessionToken           = "DELETE FROM session_token WHERE token = ?"
)
