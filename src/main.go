package main

import (
	"ak_ja/src/ui"
	"ak_ja/src/web"
)

func main() {
	go ui.InitUIServer()
	web.InitServer()
}
