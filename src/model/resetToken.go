package model

import (
	"fmt"
)

type ResetToken struct {
	Email      string
	Token      string
	ExpiryTime int64
	Used       bool
}

func (t *ResetToken) PopulateModel(m map[string]interface{}) {
	if m["email"] != nil {
		t.Email = fmt.Sprintf("%s", m["email"])
	}
	if m["token"] != nil {
		t.Token = fmt.Sprintf("%s", m["token"])
	}
	if m["expirytime"] != nil {
		expiryTime, ok := m["expirytime"].(int64)
		if ok {
			t.ExpiryTime = expiryTime
		}
	}
	if m["used"] != nil {
		if m["used"].(int64) == 1 {
			t.Used = true
		}
	}
}
