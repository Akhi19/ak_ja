package model

import (
	"fmt"
)

type User struct {
	Name         string
	Email        string
	Address      string
	Telephone    string
	Password     string
	GoogleSignIn bool
}

func (u *User) PopulateModel(m map[string]interface{}) {
	if m["name"] != nil {
		u.Name = fmt.Sprintf("%s", m["name"])
	}
	if m["email"] != nil {
		u.Email = fmt.Sprintf("%s", m["email"])
	}
	if m["address"] != nil {
		u.Address = fmt.Sprintf("%s", m["address"])
	}
	if m["telephone"] != nil {
		u.Telephone = fmt.Sprintf("%s", m["telephone"])
	}
	if m["password"] != nil {
		u.Password = fmt.Sprintf("%s", m["password"])
	}
	if m["googlesignin"] != nil {
		if m["googlesignin"].(int64) == 1 {
			u.GoogleSignIn = true
		}
	}
}
