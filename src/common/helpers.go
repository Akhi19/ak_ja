package common

import (
	"crypto/sha512"
	"encoding/hex"

	"github.com/google/uuid"
)

func GenerateRandomToken() string {
	h := sha512.New()
	h.Write([]byte(uuid.New().String()))
	token := hex.EncodeToString(h.Sum(nil))
	return token
}
