package common

import (
	"net/smtp"
)

type EmailSettings struct {
	From     string `json:"From"`
	Password string `json:"Password"`
	Server   string `json:"Server"`
	Port     string `json:"Port"`
}

type EmailSender interface {
	Send(to []string, body []byte) error
}

func NewEmailSender(conf EmailSettings) EmailSender {
	return &emailSender{conf, smtp.SendMail}
}

type emailSender struct {
	conf EmailSettings
	send func(string, smtp.Auth, string, []string, []byte) error
}

func (e *emailSender) Send(to []string, body []byte) error {
	addr := e.conf.Server + ":" + e.conf.Port
	auth := smtp.PlainAuth("", e.conf.From, e.conf.Password, e.conf.Server)
	return e.send(addr, auth, "jainakhilesh080@pepisandbox.com", to, body)
}
