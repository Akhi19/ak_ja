package common

import (
	"net/http"

	"github.com/gorilla/mux"
)

type (
	Routes struct {
		Path        string
		HandlerFunc func(http.ResponseWriter, *http.Request)
	}
)

func StartServer(addr string, routes []Routes) error {
	router := mux.NewRouter()
	for _, route := range routes {
		router.HandleFunc(route.Path, route.HandlerFunc)
	}
	return http.ListenAndServe(addr, router)
}
