package config

import (
	"ak_ja/src/common"
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"path/filepath"
	"sync"
)

const (
	configFilePath = "../Build/login_config.json"
)

type (
	GoogleAPICredentials struct {
		Id       string `json:"client_id"`
		Secret   string `json:"secret"`
		Callback string `json:"callback"`
	}

	AppAPI struct {
		ListenURL  string `json:"ListenURL"`
		URLPrefix  string `json:"URLPrefix"`
		APIVersion string `json:"APIVersion"`
	}

	UIServer struct {
		ListenURL  string `json:"ListenURL"`
		URLPrefix  string `json:"URLPrefix"`
		APIVersion string `json:"APIVersion"`
	}

	DbConnection struct {
		DriverName string `json:"Driver"`
		Username   string `json:"Username"`
		Password   string `json:"Password"`
		DbURI      string `json:"DbURI"`
		DbName     string `json:"DbName"`
	}

	Configuration struct {
		AppAPI                  AppAPI               `json:"AppAPI"`
		GoogleAPI               GoogleAPICredentials `json:"GoogleAPICredentials"`
		DbConnection            DbConnection         `json:"DbConnection"`
		UIServer                UIServer             `json:"UIServer"`
		ResetLinkExpirationSecs int                  `json:"ResetLinkExpiryTimeSecs"`
		EmailSettings           common.EmailSettings `json:"EmailSettings"`
		HostIP                  string               `json:"HostIP"`
		SessionSecretKey        string               `json:"SessionSecretKey"`
	}
)

var config *Configuration
var singleton sync.Once

func InitialiseConfig() {
	config = &Configuration{}
	buf, err := ioutil.ReadFile(filepath.Clean(configFilePath))
	if err != nil {
		panic(fmt.Errorf("Irrecoverable application state : Cannot read config file. Err : %s", err.Error()))
	}
	reader := bytes.NewBuffer(buf)
	err = json.NewDecoder(reader).Decode(config)
	if err != nil {
		panic(fmt.Errorf("Irrecoverable application state : Cannot decode config file. Err : %s", err.Error()))
	}
}

func GetConfig() *Configuration {
	singleton.Do(func() {
		InitialiseConfig()
	})
	return config
}
