package ui

import (
	"ak_ja/src/common"
	"ak_ja/src/config"
	"ak_ja/src/model"
	"encoding/json"
	"fmt"
	"html/template"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"

	"github.com/gorilla/sessions"
	"github.com/markbates/goth"
	"github.com/markbates/goth/gothic"
	"github.com/markbates/goth/providers/google"
)

func init() {
	configs := config.GetConfig()
	goth.UseProviders(
		google.New(configs.GoogleAPI.Id, configs.GoogleAPI.Secret, configs.GoogleAPI.Callback, email, "profile"),
	)
}

type FailureResponse struct {
	ErrorMessage string `json:"ErrorMessage"`
}

const (
	email           = "email"
	tokenid         = "tokenid"
	sessionTokenKey = "session"
	loginToken      = "loginToken"
	genericError    = "Unable to process request"
)

var (
	APIServerBasePath string

	store = sessions.NewCookieStore([]byte(config.GetConfig().SessionSecretKey))

	renderLoginScreen = func(w http.ResponseWriter, data interface{}) {
		executeTemplate(w, "Login Template", "./tmpl/index.html", data)
	}
	renderForgotPasswordScreen = func(w http.ResponseWriter, data interface{}) {
		executeTemplate(w, "Forgot Template", "./tmpl/forgotpassword.html", data)
	}
	renderProfileScreen = func(w http.ResponseWriter, data interface{}) {
		executeTemplate(w, "Profile Template", "./tmpl/profile.html", data)
	}
	renderEditScreen = func(w http.ResponseWriter, data interface{}) {
		executeTemplate(w, "Edit Profile Template", "./tmpl/editprofile.html", data)
	}
	renderRegisterScreen = func(w http.ResponseWriter, data interface{}) {
		executeTemplate(w, "Register Profile Template", "./tmpl/registerprofile.html", data)
	}
	renderPasswordResetScreen = func(w http.ResponseWriter, data interface{}) {
		executeTemplate(w, "Reset Template", "./tmpl/resetpassword.html", data)
	}
	renderErrorPage = func(w http.ResponseWriter, data interface{}) {
		executeTemplate(w, "Error Page", "./tmpl/errorpage.html", data)
	}
)

func setResponseHeaders(w http.ResponseWriter) {
	w.Header().Set("Cache-Control", "no-cache, no-store, must-revalidate") // HTTP 1.1.
	w.Header().Set("Pragma", "no-cache")                                   // HTTP 1.0.
	w.Header().Set("Expires", "0")                                         // Proxies.
}

func loginScreen(w http.ResponseWriter, req *http.Request) {
	setResponseHeaders(w)
	user := getSessionData(req)
	if user != nil {
		renderProfileScreen(w, user)
		return
	}
	renderLoginScreen(w, "")
}

func registerScreen(w http.ResponseWriter, req *http.Request) {
	setResponseHeaders(w)
	user := getSessionData(req)
	if user != nil {
		renderProfileScreen(w, user)
		return
	}
	renderRegisterScreen(w, "")
}

func signIn(w http.ResponseWriter, req *http.Request) {
	setResponseHeaders(w)
	user := getSessionData(req)
	if user != nil {
		renderProfileScreen(w, user)
		return
	}
	reqBody, err := ioutil.ReadAll(req.Body)
	if err != nil {
		renderLoginScreen(w, genericError)
		return
	}
	apiReq, err := createRequest(http.MethodGet, APIServerBasePath+"/signin", reqBody)
	if err != nil {
		renderLoginScreen(w, "")
		return
	}
	sessToken := common.GenerateRandomToken()
	apiReq.Header.Add(sessionTokenKey, sessToken)
	res, err := executeRequest(apiReq)
	if err != nil {
		renderLoginScreen(w, "")
		return
	}
	userModel := &model.User{}
	err = extractResponseBody(res.Body, userModel)
	if err != nil {
		renderLoginScreen(w, genericError)
		return
	}
	saveCookie(req, w, sessToken)
	renderProfileScreen(w, userModel)
}

func userLogout(w http.ResponseWriter, req *http.Request) {
	setResponseHeaders(w)
	deleteCookie(req, w)
	req, err := createRequest(http.MethodGet, APIServerBasePath+"/logout", nil)
	if err != nil {
		renderErrorPage(w, "")
		return
	}
	_, err = executeRequest(req)
	if err != nil {
		renderErrorPage(w, "")
		return
	}
	renderLoginScreen(w, "")
}

func registerUser(w http.ResponseWriter, req *http.Request) {
	setResponseHeaders(w)
	user := getSessionData(req)
	if user != nil {
		renderProfileScreen(w, user)
		return
	}
	reqBody, err := ioutil.ReadAll(req.Body)
	if err != nil {
		renderLoginScreen(w, genericError)
		return
	}
	apiReq, err := createRequest(http.MethodPost, APIServerBasePath+"/registeruser", reqBody)
	if err != nil {
		renderLoginScreen(w, genericError)
		return
	}
	sessToken := common.GenerateRandomToken()
	apiReq.Header.Add(sessionTokenKey, sessToken)
	res, err := executeRequest(apiReq)
	if err != nil {
		renderLoginScreen(w, genericError)
		return
	}
	userModel := &model.User{}
	err = extractResponseBody(res.Body, userModel)
	if err != nil {
		renderLoginScreen(w, genericError)
		return
	}
	saveCookie(req, w, sessToken)
	renderEditScreen(w, userModel)
}

func updateProfile(w http.ResponseWriter, req *http.Request) {
	setResponseHeaders(w)
	url := APIServerBasePath + "/updateprofile?" + req.URL.RawQuery
	reqBody, err := ioutil.ReadAll(req.Body)
	if err != nil {
		renderEditScreen(w, genericError)
		return
	}
	apiReq, err := createRequest(http.MethodPost, url, reqBody)
	if err != nil {
		renderEditScreen(w, genericError)
		return
	}
	apiReq.Header.Add(sessionTokenKey, sessionToken(req))
	res, err := executeRequest(apiReq)
	if err != nil {
		renderEditScreen(w, genericError)
		return
	}
	if res.StatusCode == http.StatusUnauthorized {
		renderLoginScreen(w, "Could not authorize session")
		return
	}
	userModel := &model.User{}
	err = extractResponseBody(res.Body, userModel)
	if err != nil {
		renderRegisterScreen(w, genericError)
		return
	}
	renderProfileScreen(w, userModel)
}

func editProfile(w http.ResponseWriter, req *http.Request) {
	setResponseHeaders(w)
	url := APIServerBasePath + "/editprofile?" + req.URL.RawQuery
	apiReq, err := createRequest(http.MethodGet, url, nil)
	if err != nil {
		renderEditScreen(w, genericError)
		return
	}
	apiReq.Header.Add(sessionTokenKey, sessionToken(req))
	res, err := executeRequest(apiReq)
	if err != nil {
		renderEditScreen(w, genericError)
		return
	}
	if res.StatusCode == http.StatusUnauthorized {
		renderLoginScreen(w, "Could not authorize session")
		return
	}
	userModel := &model.User{}
	err = extractResponseBody(res.Body, userModel)
	if err != nil {
		renderRegisterScreen(w, genericError)
		return
	}
	renderEditScreen(w, userModel)
}

func forgotPasswordPage(w http.ResponseWriter, req *http.Request) {
	setResponseHeaders(w)
	renderForgotPasswordScreen(w, "")
}

func forgotPassword(w http.ResponseWriter, req *http.Request) {
	setResponseHeaders(w)
	reqBody, err := ioutil.ReadAll(req.Body)
	if err != nil {
		renderLoginScreen(w, genericError)
		return
	}
	apiReq, err := createRequest(http.MethodPost, APIServerBasePath+"/forgotpassword", reqBody)
	if err != nil {
		renderLoginScreen(w, genericError)
		return
	}
	res, err := executeRequest(apiReq)
	if err != nil || res.StatusCode != http.StatusOK {
		renderLoginScreen(w, genericError)
		return
	}
	renderLoginScreen(w, "Reset link sent on mail")
}

func resetPasswordPage(w http.ResponseWriter, req *http.Request) {
	setResponseHeaders(w)
	m, err := getUrlParams(req.URL.RawQuery)
	if err != nil || len(m[email]) == 0 || len(m[tokenid]) == 0 {
		renderLoginScreen(w, genericError)
		return
	}
	renderPasswordResetScreen(w, map[string]interface{}{"tokenid": m[tokenid][0], "email": m[email][0]})
}

func resetPassword(w http.ResponseWriter, req *http.Request) {
	setResponseHeaders(w)
	reqBody, err := ioutil.ReadAll(req.Body)
	if err != nil {
		renderLoginScreen(w, genericError)
		return
	}
	m, err := getUrlParams(req.URL.RawQuery)
	if err != nil || len(m[email]) == 0 || len(m[tokenid]) == 0 {
		renderLoginScreen(w, genericError)
		return
	}
	url := APIServerBasePath + fmt.Sprintf("/resetpassword?tokenid=%s&email=%s", m[tokenid][0], m[email][0])
	apiReq, err := createRequest(http.MethodPost, url, reqBody)
	if err != nil {
		renderLoginScreen(w, genericError)
		return
	}
	res, err := executeRequest(apiReq)
	if err != nil || res.StatusCode != http.StatusOK {
		renderLoginScreen(w, genericError)
		return
	}
	renderLoginScreen(w, "Password Updated Successfully")
}

func googleLogin(w http.ResponseWriter, req *http.Request) {
	setResponseHeaders(w)
	gothic.BeginAuthHandler(w, req)
}

func googleCallback(w http.ResponseWriter, req *http.Request) {
	setResponseHeaders(w)
	user, err := gothic.CompleteUserAuth(w, req)
	if err != nil {
		renderLoginScreen(w, genericError)
		return
	}
	userObject, err := json.Marshal(user)
	if err != nil {
		renderLoginScreen(w, genericError)
		return
	}
	apiReq, err := createRequest(http.MethodPost, APIServerBasePath+"/googlesignin", userObject)
	if err != nil {
		renderLoginScreen(w, genericError)
		return
	}
	sessToken := common.GenerateRandomToken()
	apiReq.Header.Add(sessionTokenKey, sessToken)
	res, err := executeRequest(apiReq)
	if err != nil || (res.StatusCode != http.StatusNoContent && res.StatusCode != http.StatusOK) {
		renderLoginScreen(w, genericError)
		return
	}
	if res.StatusCode == http.StatusNoContent {
		saveCookie(req, w, sessToken)
		renderEditScreen(w, map[string]interface{}{"Email": user.Email, "GoogleSignIn": true})
		return
	}
	userModel := &model.User{}
	err = extractResponseBody(res.Body, userModel)
	if err != nil {
		renderLoginScreen(w, genericError)
		return
	}
	saveCookie(req, w, sessToken)
	renderProfileScreen(w, userModel)
}

func executeTemplate(w http.ResponseWriter, templateName, templateFile string, data interface{}) {
	//can be stored once and use globally
	t := template.New(templateName)
	b, err := ioutil.ReadFile(templateFile)
	if err != nil {
		returnFailureResponse(w, http.StatusBadRequest, genericError)
		return
	}
	t = template.Must(t.Parse(string(b)))
	t.ExecuteTemplate(w, templateName, data)
}

func returnFailureResponse(w http.ResponseWriter, statusCode int, message string) {
	failure := &FailureResponse{ErrorMessage: message}
	msg, _ := json.Marshal(failure)
	w.WriteHeader(statusCode)
	w.Write(msg)
}

func extractResponseBody(body io.ReadCloser, v interface{}) error {
	resBody, err := ioutil.ReadAll(body)
	if err != nil {
		return err
	}
	return json.Unmarshal(resBody, v)
}

func getUrlParams(query string) (map[string][]string, error) {
	m, err := url.ParseQuery(query)
	if err != nil {
		return nil, err
	}
	if len(m) == 0 {
		return nil, fmt.Errorf(genericError)
	}
	return m, nil
}

func sessionToken(req *http.Request) string {
	session, err := store.Get(req, "ak_ja")
	if err != nil {
		return ""
	}
	if token, ok := session.Values[loginToken].(string); ok && token != "" {
		return token
	}
	return ""
}

func getSessionData(req *http.Request) *model.User {
	token := sessionToken(req)
	if token == "" {
		return nil
	}
	apiReq, err := createRequest(http.MethodGet, APIServerBasePath+"/getuser", nil)
	if err != nil {
		return nil
	}
	apiReq.Header.Add(sessionTokenKey, token)
	res, err := executeRequest(apiReq)
	if err != nil && res.StatusCode != http.StatusOK {
		return nil
	}
	userModel := &model.User{}
	err = extractResponseBody(res.Body, userModel)
	if err != nil {
		return nil
	}
	return userModel
}

func saveCookie(req *http.Request, w http.ResponseWriter, token string) {
	session, err := store.Get(req, "ak_ja")
	if err != nil {
		return
	}
	session.Values[loginToken] = token
	session.Save(req, w)
}

func deleteCookie(req *http.Request, w http.ResponseWriter) {
	session, err := store.Get(req, "ak_ja")
	if err != nil {
		return
	}
	session.Values[loginToken] = ""
	session.Save(req, w)
}
