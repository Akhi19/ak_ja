package ui

import (
	"ak_ja/src/common"
	"ak_ja/src/config"
	"fmt"
)

func getRoutes(config *config.Configuration) ([]common.Routes, error) {
	basePath := config.UIServer.URLPrefix + config.UIServer.APIVersion
	return []common.Routes{
		common.Routes{
			Path:        basePath,
			HandlerFunc: loginScreen,
		},
		common.Routes{
			Path:        basePath + "/signin",
			HandlerFunc: signIn,
		},
		common.Routes{
			Path:        basePath + "/signUp",
			HandlerFunc: registerScreen,
		},
		common.Routes{
			Path:        basePath + "/logout",
			HandlerFunc: userLogout,
		},
		common.Routes{
			Path:        basePath + "/auth/{provider}",
			HandlerFunc: googleLogin,
		},
		common.Routes{
			Path:        basePath + "/auth/{provider}/callback",
			HandlerFunc: googleCallback,
		},
		common.Routes{
			Path:        basePath + "/registeruser",
			HandlerFunc: registerUser,
		},
		common.Routes{
			Path:        basePath + "/updateprofile",
			HandlerFunc: updateProfile,
		},
		common.Routes{
			Path:        basePath + "/editprofile",
			HandlerFunc: editProfile,
		},
		common.Routes{
			Path:        basePath + "/forgotpasswordpage",
			HandlerFunc: forgotPasswordPage,
		},
		common.Routes{
			Path:        basePath + "/forgotpassword",
			HandlerFunc: forgotPassword,
		},
		common.Routes{
			Path:        basePath + "/resetpasswordpage",
			HandlerFunc: resetPasswordPage,
		},
		common.Routes{
			Path:        basePath + "/resetpassword",
			HandlerFunc: resetPassword,
		},
	}, nil
}

func InitUIServer() {
	config := config.GetConfig()
	APIServerBasePath = config.HostIP + config.AppAPI.ListenURL + config.AppAPI.URLPrefix +
		config.AppAPI.APIVersion
	routes, err := getRoutes(config)
	if err != nil {
		panic(fmt.Errorf("Irrecoverable : Could not start server. Reason : %s", err.Error()))
	}
	err = common.StartServer(config.UIServer.ListenURL, routes)
	if err != nil {
		panic(fmt.Errorf("Irrecoverable : Could not start server. Reason : %s", err.Error()))
	}
}
