package ui

import (
	"bytes"
	"net/http"
)

func createRequest(reqMethod string, url string, body []byte) (*http.Request, error) {
	return http.NewRequest(reqMethod, url, bytes.NewBuffer(body))
}

func executeRequest(req *http.Request) (*http.Response, error) {
	client := http.DefaultClient
	return client.Do(req)
}
