package dal

import (
	"ak_ja/src/config"
	"fmt"
)

//DbAccessor is abstract access layer for underlying persistent store
type DbAccessor interface {
	InsertQuery(queryTemplate string, values ...interface{}) error
	UpdateQuery(queryTemplate string, values ...interface{}) error
	DeleteQuery(queryTemplate string, values ...interface{}) error
	SelectQuery(queryTemplate string, values ...interface{}) ([]map[string]interface{}, error)
}

func ConnectDB(config *config.Configuration) (DbAccessor, error) {
	switch config.DbConnection.DriverName {
	case "mysql":
		return mysqlDbAccessor(config)
	default:
		return nil, fmt.Errorf("Db Driver not found")
	}
}
