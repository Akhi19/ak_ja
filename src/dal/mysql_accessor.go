package dal

import (
	"ak_ja/src/config"
	"database/sql"
	"errors"
	"sync"

	_ "github.com/go-sql-driver/mysql"
)

type MysqlAccessor struct {
	dbProvider *sql.DB
}

var sqlDbAccessor *MysqlAccessor
var mu sync.Mutex

func mysqlDbAccessor(config *config.Configuration) (DbAccessor, error) {
	mu.Lock()
	if sqlDbAccessor == nil {
		dataSourceName := config.DbConnection.Username + ":" + config.DbConnection.Password + "@tcp(" +
			config.DbConnection.DbURI + ")/" + config.DbConnection.DbName
		db, err := sql.Open("mysql", dataSourceName)
		if err != nil {
			return nil, err
		}
		sqlDbAccessor = &MysqlAccessor{
			dbProvider: db,
		}
	}
	mu.Unlock()
	return sqlDbAccessor, nil
}

func (db *MysqlAccessor) InsertQuery(queryTemplate string, values ...interface{}) error {
	_, err := db.dbProvider.Query(queryTemplate, values...)
	return err
}

func (db *MysqlAccessor) UpdateQuery(queryTemplate string, values ...interface{}) error {
	_, err := db.dbProvider.Query(queryTemplate, values...)
	return err
}

func (db *MysqlAccessor) DeleteQuery(queryTemplate string, values ...interface{}) error {
	_, err := db.dbProvider.Query(queryTemplate, values...)
	return err
}

func (db *MysqlAccessor) SelectQuery(queryTemplate string, values ...interface{}) ([]map[string]interface{}, error) {
	rows, err := db.dbProvider.Query(queryTemplate, values...)
	if err != nil {
		return nil, err
	}
	return convertSQLRowsToMap(rows)
}

func convertSQLRowsToMap(rows *sql.Rows) ([]map[string]interface{}, error) {
	err := errors.New("Invalid sql rows")
	count := 0
	if rows == nil {
		return nil, err
	}
	rowsHolder := make([]map[string]interface{}, 0, 1)
	defer rows.Close()
	for rows.Next() {
		count++
		cols, err := rows.Columns()
		if err != nil {
			return nil, err
		}
		columns := make([]interface{}, len(cols))
		columnPointers := make([]interface{}, len(cols))
		for i := range columns {
			columnPointers[i] = &columns[i]
		}
		err = rows.Scan(columnPointers...)
		if err != nil {
			return nil, err
		}
		m := make(map[string]interface{})
		for i, colName := range cols {
			val := columnPointers[i].(*interface{})
			m[colName] = *val
		}
		rowsHolder = append(rowsHolder, m)
	}
	return rowsHolder, nil
}
