package web

import (
	"errors"
	"net/url"
	"regexp"
)

var emailRegex = regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+\\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")

func validateRegisterUser(urlValues url.Values) (map[string]interface{}, error) {
	m := make(map[string]interface{})
	m["password"] = urlValues["password"][0]
	if m["password"] == nil || m["password"] == "" {
		return nil, errors.New("Please provide password")
	}
	m["email"] = urlValues["email"][0]
	if m["email"] == nil || m["email"] == "" || !emailRegex.MatchString(m["email"].(string)) {
		return nil, errors.New("Please provide valid email")
	}
	return m, nil
}

func validateUserProfile(urlValues url.Values) (map[string]interface{}, error) {
	m := make(map[string]interface{})
	m["name"] = urlValues["name"][0]
	if m["name"] == nil || m["name"] == "" {
		return nil, errors.New("Please provide name")
	}
	m["address"] = urlValues["address"][0]
	if m["address"] == nil || m["address"] == "" {
		return nil, errors.New("Please provide address")
	}
	m["telephone"] = urlValues["telephone"][0]
	if m["telephone"] == nil || m["telephone"] == "" {
		return nil, errors.New("Please provide telephone")
	}
	m["email"] = urlValues["email"][0]
	if m["email"] == nil || m["email"] == "" || !emailRegex.MatchString(m["email"].(string)) {
		return nil, errors.New("Please provide valid email")
	}
	return m, nil
}
