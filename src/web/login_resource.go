package web

import (
	"ak_ja/src/config"
	"ak_ja/src/model"
	"ak_ja/src/service"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
)

const (
	email           = "email"
	password        = "password"
	tokenid         = "tokenid"
	sessionTokenKey = "session"

	genericError = "Unable to process request"
)

type (
	Handler struct {
		configs *config.Configuration
		service *service.LoginService
	}
)

func (h *Handler) sessionValidator(routeHandler http.HandlerFunc, sessionCheck, sessionNeeded bool) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		if sessionCheck {
			sessionToken := req.Header.Get(sessionTokenKey)
			_, err := h.service.GetUserFromSessionToken(sessionToken)
			if err != nil && sessionNeeded {
				returnResponse(w, http.StatusUnauthorized, "Unable to authorize request")
				return
			}
			routeHandler(w, req)
			return
		}
		routeHandler(w, req)
	}
}

func (h *Handler) logout(w http.ResponseWriter, req *http.Request) {
	h.deleteSessionToken(req.Header.Get(sessionTokenKey))
}

func (h *Handler) login(w http.ResponseWriter, req *http.Request) {
	m, err := getPayloadMap(req)
	if err != nil || len(m[email]) == 0 || len(m[password]) == 0 {
		returnResponse(w, http.StatusBadRequest, genericError)
		return
	}
	user, err := h.service.AuthenticateAndGetUser(fmt.Sprintf("%s", m[email][0]), fmt.Sprintf("%s", m[password][0]))
	if err != nil {
		returnResponse(w, http.StatusBadRequest, genericError)
		return
	}
	if user.Name == "" {
		returnResponse(w, http.StatusBadRequest, genericError)
		return
	}
	h.saveSessionToken(req.Header.Get(sessionTokenKey), user)
	returnResponse(w, http.StatusOK, user)
}

func (h *Handler) getUser(w http.ResponseWriter, req *http.Request) {
	sessionToken := req.Header.Get(sessionTokenKey)
	user, err := h.service.GetUserFromSessionToken(sessionToken)
	if err != nil {
		returnResponse(w, http.StatusBadRequest, genericError)
		return
	}
	if user.Name == "" {
		h.deleteSessionToken(sessionToken)
		returnResponse(w, http.StatusBadRequest, genericError)
		return
	}
	returnResponse(w, http.StatusOK, user)
}

func (h *Handler) registerUser(w http.ResponseWriter, req *http.Request) {
	m, err := getPayloadMap(req)
	if err != nil {
		returnResponse(w, http.StatusBadRequest, genericError)
		return
	}
	validatedParams, err := validateRegisterUser(m)
	if err != nil {
		returnResponse(w, http.StatusBadRequest, genericError)
		return
	}
	err = h.service.SignUp(validatedParams, false)
	if err != nil {
		returnResponse(w, http.StatusInternalServerError, genericError)
		return
	}
	userModel := &model.User{}
	userModel.PopulateModel(validatedParams)
	h.saveSessionToken(req.Header.Get(sessionTokenKey), userModel)
	returnResponse(w, http.StatusOK, userModel)
}

func (h *Handler) updateProfile(w http.ResponseWriter, req *http.Request) {
	m, err := getPayloadMap(req)
	if err != nil {
		returnResponse(w, http.StatusBadRequest, genericError)
		return
	}
	urlParams, err := getUrlParams(req.URL.RawQuery)
	if err != nil || len(urlParams[email]) == 0 {
		returnResponse(w, http.StatusBadRequest, genericError)
		return
	}
	currentEmail := urlParams[email][0]
	validatedParams, err := validateUserProfile(m)
	if err != nil {
		returnResponse(w, http.StatusBadRequest, genericError)
		return
	}
	err = h.service.UpdateProfile(validatedParams, currentEmail)
	if err != nil {
		returnResponse(w, http.StatusInternalServerError, genericError)
		return
	}
	userModel := &model.User{}
	userModel.PopulateModel(validatedParams)
	if currentEmail != validatedParams[email] {
		h.deleteSessionToken(req.Header.Get(sessionTokenKey))
		h.saveSessionToken(req.Header.Get(sessionTokenKey), userModel)
	}
	returnResponse(w, http.StatusOK, userModel)
}

func (h *Handler) editProfile(w http.ResponseWriter, req *http.Request) {
	m, err := getUrlParams(req.URL.RawQuery)
	if err != nil || len(m[email]) == 0 {
		returnResponse(w, http.StatusBadRequest, genericError)
		return
	}
	user, err := h.service.GetUser(m[email][0])
	if err != nil {
		returnResponse(w, http.StatusBadRequest, genericError)
		return
	}
	returnResponse(w, http.StatusOK, user)
}

func (h *Handler) googleSignIn(w http.ResponseWriter, req *http.Request) {
	reqBody, err := ioutil.ReadAll(req.Body)
	if err != nil {
		returnResponse(w, http.StatusInternalServerError, genericError)
		return
	}
	m := make(map[string]interface{})
	err = json.Unmarshal(reqBody, &m)
	if err != nil {
		returnResponse(w, http.StatusInternalServerError, genericError)
		return
	}
	email, ok := m["Email"].(string)
	if !ok {
		returnResponse(w, http.StatusInternalServerError, genericError)
		return
	}
	userModel, err := h.service.GetUser(email)
	if err != nil && err.Error() != "Email does not exist" {
		returnResponse(w, http.StatusBadRequest, nil)
		return
	}
	userModel.GoogleSignIn = true
	if userModel.Name == "" {
		if userModel.Email == "" {
			err := h.service.SignUp(map[string]interface{}{"email": email, "password": m["AccessToken"]}, true)
			if err != nil {
				returnResponse(w, http.StatusInternalServerError, "Could not register user")
				return
			}
		}
		h.saveSessionToken(req.Header.Get(sessionTokenKey), userModel)
		returnResponse(w, http.StatusNoContent, nil)
		return
	}
	h.saveSessionToken(req.Header.Get(sessionTokenKey), userModel)
	returnResponse(w, http.StatusOK, userModel)
}

func (h *Handler) forgotPassword(w http.ResponseWriter, req *http.Request) {
	m, err := getPayloadMap(req)
	if err != nil || len(m[email]) == 0 {
		returnResponse(w, http.StatusBadRequest, genericError)
		return
	}
	err = h.service.ForgotPassword(fmt.Sprintf("%s", m[email][0]))
	if err != nil {
		returnResponse(w, http.StatusInternalServerError, genericError)
		return
	}
	returnResponse(w, http.StatusOK, "Reset Link sent on email")
}

func (h *Handler) resetPassword(w http.ResponseWriter, req *http.Request) {
	m, err := getUrlParams(req.URL.RawQuery)
	if err != nil || len(m[tokenid]) == 0 || len(m[email]) == 0 {
		returnResponse(w, http.StatusBadRequest, genericError)
		return
	}
	err = h.service.VerifyToken(m[tokenid][0], m[email][0])
	if err != nil {
		returnResponse(w, http.StatusBadRequest, genericError)
		return
	}
	payload, err := getPayloadMap(req)
	if err != nil || len(payload[password]) == 0 {
		returnResponse(w, http.StatusBadRequest, genericError)
		return
	}
	err = h.service.UpdatePassword(m[email][0], payload[password][0])
	if err != nil {
		returnResponse(w, http.StatusInternalServerError, genericError)
		return
	}
	returnResponse(w, http.StatusOK, "Password successfully updated")
}

func getUrlParams(query string) (map[string][]string, error) {
	m, err := url.ParseQuery(query)
	if err != nil {
		return nil, err
	}
	if len(m) == 0 {
		return nil, fmt.Errorf(genericError)
	}
	return m, nil
}

func getPayloadMap(req *http.Request) (map[string][]string, error) {
	reqBody, err := ioutil.ReadAll(req.Body)
	if err != nil {
		return nil, err
	}
	payload, err := url.ParseQuery(string(reqBody))
	if err != nil {
		return nil, err
	}
	if len(payload) == 0 {
		return nil, fmt.Errorf(genericError)
	}
	return payload, nil
}

func (h *Handler) saveSessionToken(token string, user *model.User) {
	h.service.SaveSessionToken(token, user.Email)
}

func (h *Handler) deleteSessionToken(token string) {
	h.service.DeleteSessionToken(token)
}

func returnResponse(w http.ResponseWriter, statusCode int, data interface{}) {
	msg, err := json.Marshal(data)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Something went wrong"))
	}
	w.WriteHeader(statusCode)
	w.Write(msg)
}
