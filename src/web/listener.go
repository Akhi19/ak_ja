package web

import (
	"ak_ja/src/common"
	"ak_ja/src/config"
	"ak_ja/src/service"
	"fmt"
)

func getRoutes(config *config.Configuration) ([]common.Routes, error) {
	basePath := config.AppAPI.URLPrefix + config.AppAPI.APIVersion
	loginService, err := service.GetLoginService()
	if err != nil {
		return nil, err
	}
	loginHandler := &Handler{
		configs: config,
		service: loginService,
	}
	return []common.Routes{
		common.Routes{
			Path:        basePath + "/signin",
			HandlerFunc: loginHandler.sessionValidator(loginHandler.login, true, false),
		},
		common.Routes{
			Path:        basePath + "/getuser",
			HandlerFunc: loginHandler.sessionValidator(loginHandler.getUser, true, false),
		},
		common.Routes{
			Path:        basePath + "/logout",
			HandlerFunc: loginHandler.sessionValidator(loginHandler.logout, false, false),
		},
		common.Routes{
			Path:        basePath + "/googlesignin",
			HandlerFunc: loginHandler.sessionValidator(loginHandler.googleSignIn, false, false),
		},
		common.Routes{
			Path:        basePath + "/registeruser",
			HandlerFunc: loginHandler.sessionValidator(loginHandler.registerUser, false, false),
		},
		common.Routes{
			Path:        basePath + "/updateprofile",
			HandlerFunc: loginHandler.sessionValidator(loginHandler.updateProfile, true, true),
		},
		common.Routes{
			Path:        basePath + "/editprofile",
			HandlerFunc: loginHandler.sessionValidator(loginHandler.editProfile, true, true),
		},
		common.Routes{
			Path:        basePath + "/forgotpassword",
			HandlerFunc: loginHandler.sessionValidator(loginHandler.forgotPassword, false, false),
		},
		common.Routes{
			Path:        basePath + "/resetpassword",
			HandlerFunc: loginHandler.sessionValidator(loginHandler.resetPassword, false, false),
		},
	}, nil
}

func InitServer() {
	config := config.GetConfig()
	routes, err := getRoutes(config)
	if err != nil {
		panic(fmt.Errorf("Irrecoverable : Could not start server. Reason : %s", err.Error()))
	}
	err = common.StartServer(config.AppAPI.ListenURL, routes)
	if err != nil {
		panic(fmt.Errorf("Irrecoverable : Could not start server. Reason : %s", err.Error()))
	}
}
