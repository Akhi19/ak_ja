module ak_ja

go 1.12

require (
	github.com/go-sql-driver/mysql v1.5.0
	github.com/golang/groupcache v0.0.0-20200121045136-8c9f03a8e57e // indirect
	github.com/golang/mock v1.4.4
	github.com/google/uuid v1.1.2
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/sessions v1.2.1
	github.com/markbates/goth v1.66.1
	github.com/stretchr/testify v1.4.0
	google.golang.org/protobuf v1.25.0 // indirect
)
